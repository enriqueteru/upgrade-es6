
//1

let sum = (a = 10, b =5) => a+b ;

console.log('1.1:  ' + sum());
console.log('1.2:  ' + sum(10));
console.log('1.3:  ' + sum(10,5));


//2

// 2.1 destructuring object
const game = {title: 'The last us 2', gender: ['action', 'zombie', 'survival'], year: 2020};

let{title, gender, year} = game;
console.log("Game:   " + title);
console.log("Genders: " + gender);
console.log("Year:   " + year);


const fruits = ['Banana', 'Strawberry', 'Orange'];

//2.2 destructuring array
let [fruit1, fruit2, fruit3] = fruits;

console.log(fruit1);
console.log(fruit2);
console.log(fruit3);

//2.3 destructuring function return
const animalFunction = () => {
    return {name: 'Bengal Tiger', race: 'Tiger'}
};


let {name, race} = animalFunction();
console.log(name);
console.log(race);


//2.4
const car = {nameCar: 'Mazda 6', itv: [2015, 2011, 2020]};

//decalro valor de key y nuevo array
let {nameCar, itv} = car;

//destructuro el nuevo array
let [year1, year2, year3] = itv;


//Iteración #3: Spread Operator

//3.1
const pointsList = [32, 54, 21, 64, 75, 43]
let newPointList = [...pointsList];
console.log(newPointList)

//3.2
const toy = {name: 'Bus laiyiar', date: '20-30-1995', color: 'multicolor'};
let newToy = {...toy}
console.log(newToy);

//3.3

const pointsList2 = [32, 54, 21, 64, 75, 43];
const pointsLis3 = [54,87,99,65,32];
let joinPointLists = [...pointsList2, ...pointsLis3]
console.log(joinPointLists);

//3.4
const toy2 = {name: 'Bus laiyiar', date: '20-30-1995', color: 'multicolor'};
const toyUpdate = {lights: 'rgb', power: ['Volar like a dragon', 'MoonWalk']}

let newObj = {...toy2, ...toyUpdate};
console.log(newObj)
console.log(newObj.power)

const colors = ['rojo', 'azul', 'amarillo', 'verde', 'naranja'];

let newColors = [];
let amarillo = [...colors].splice(2,1);
//colors.splice(2,1) Manera rápida

for (color of colors) {
if (color !== amarillo[0]){
newColors.push(color)}}; 


console.log(newColors)