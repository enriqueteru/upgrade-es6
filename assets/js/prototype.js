//4.1

const users = [
	{id: 1, name: 'Abel'},
	{id:2, name: 'Julia'},
	{id:3, name: 'Pedro'},
	{id:4, name: 'Amanda'}
];

let nameUsers = users.map(e => e.name)
console.log(nameUsers);


//4.2
const users2 = [
	{id: 1, name: 'Abel'},
	{id:2, name: 'Julia'},
	{id:3, name: 'Pedro'},
	{id:4, name: 'Amanda'}
];

let userName2 = users2.map(element => element.name)
console.log(userName2.includes('A'));


//4.3 
const cities = [
	{isVisited:true, name: 'Tokyo'}, 
	{isVisited:false, name: 'Madagascar'},
	{isVisited:true, name: 'Amsterdam'}, 
	{isVisited:false, name: 'Seul'}
];


let visited = cities.map( element => {
    let e = Object.assign({}, element);
    if (e.isVisited === true) {
        return e.name = e.name + '(visited)'
    }else {
        return e.name }
    });


console.log(visited);


//Iteración #5: Filter
//5.1
const ages = [22, 14, 24, 55, 65, 21, 12, 13, 90];

const filter = ages.filter(element => element > 18);
console.log(filter)

//5.2 
const par = ages.filter(element => element%2 == 0)
console.log(par);

//5.3


const streamers = [
	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'}, 
	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
];

const lol = streamers.filter(lollers => lollers.gameMorePlayed === "League of Legends")
console.log(lol);



//Iteración #6: Find

const numbers5 = [32, 21, 63, 95, 100, 67, 43];

const find = numbers5.find(number => number === 100);
console.log(find);


const movies5 = [
	{title: 'Madagascar', stars: 4.5, date: 2015},
	{title: 'Origen', stars: 5, date: 2010},
	{title: 'Your Name', stars: 5, date: 2016}
];

const findMovie = movies5.find(element => element.date === 2010);
console.log(findMovie)


const aliens = [
	{name: 'Zalamero', planet: 'Eden', age: 4029},
	{name: 'Paktu', planet: 'Andromeda', age: 32},
	{name: 'Cucushumushu', planet: 'Marte', age: 503021}
];
const mutations = [
	{name: 'Porompompero', description: 'Hace que el alien pueda adquirir la habilidad de tocar el tambor'},
	{name: 'Fly me to the moon', description: 'Permite volar, solo y exclusivamente a la luna'},
	{name: 'Andando que es gerundio', description: 'Invoca a un señor mayor como Personal Trainer'}
];


const cucushumushu = aliens.find(alien => alien.name === "Cucushumushu");
const porompompero = mutations.find(mutation => mutation.name === "Porompompero");

const mutationmax = {...cucushumushu, ...porompompero};

console.log(mutationmax);


//Iteración #7: Reduce


const exams = [
    {name: 'Yuyu Cabeza Crack', score: 5}, 
    {name: 'Maria Aranda Jimenez', score: 1}, 
    {name: 'Cristóbal Martínez Lorenzo', score: 6}, 
    {name: 'Mercedez Regrera Brito', score: 7},
    {name: 'Pamela Anderson', score: 3},
    {name: 'Enrique Perez Lijó', score: 6},
    {name: 'Pedro Benitez Pacheco', score: 8},
    {name: 'Ayumi Hamasaki', score: 4},
    {name: 'Robert Kiyosaki', score: 2},
    {name: 'Keanu Reeves', score: 10}
];


const reducer = (previousValue, currentValue) => previousValue + currentValue;
console.log(exams.score.reduce(reducer));